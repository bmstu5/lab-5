DEBUG_FLAGS = -Wall -Wextra -Wmismatched-dealloc -Wmismatched-new-delete -Wfree-nonheap-object
SRC_FILES = src/main.cpp src/sort.cpp src/sort.h

all: release

run: release
	bin/release/work

clean:
	rm -f bin/debug/* bin/release/*

release: bin/release/ $(SRC_FILES)
	g++ -o bin/release/work $(SRC_FILES) $(DEBUG_FLAGS)

debug: bin/debug/ $(SRC_FILES)
	g++ -g -o bin/debug/work $(SRC_FILES) $(DEBUG_FLAGS)

bin/release/:
	mkdir -p bin/release/

bin/debug/:
	mkdir -p bin/debug/