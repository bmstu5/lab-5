#ifndef SORT_H
#define SORT_H

#include <iostream>

enum Logging { no_logs, log_result, log_each_swap };

int* printArr(int* arr, int size);
int* maxSort(int* arr, const int size, Logging logging, int& checked, int& swapped);
int* bubbleSort(int* arr, const int size, Logging logging, int& checked, int& swapped);
int* reverse(int* arr, int size, Logging logging);

#endif