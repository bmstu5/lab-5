#include "sort.h"

int* printArr(int* const arr, const int size)
{
	int* darr = arr;
	std::cout << "  [";
	if (size >= 1) {
		std::cout << *(darr++); // print first element
	}
	for (int i = 1; i < size; i++) {
		std::cout << ", " << *(darr++); // print next elements
	}
	std::cout << "]\n";

	return arr;
}

int* maxSort(int* arr, const int size, Logging logging, int& checked, int& swapped)
{
	checked = 0;
	swapped = 0;
	int dsize = size;

	while (dsize >= 2) {
        // find maximum element
		int* pmax = arr;
		for (int k = 0; k+1 < dsize; k++) {
			checked++;
			if (*pmax < arr[k+1])
				pmax = arr + k + 1;
		}
		swapped++;
		std::swap(*pmax, arr[--dsize]);

		if (logging == log_each_swap)
			printArr(arr, size);
	}

	return arr;
}

int* bubbleSort(int* const arr, const int size, Logging logging, int& checked, int& swapped)
{
	checked = 0;
	swapped = 0;
	int k;
	int buffer;
	int dsize = size;
	
	do {
		buffer = swapped;
		for (k = 0; k+1 < dsize; k++) {
			checked++;
			if (arr[k] > arr[k+1]) {
				swapped++;
				std::swap(arr[k], arr[k+1]);

				if (logging == log_each_swap)
					printArr(arr, size);
			}
		}
	} while (buffer != swapped && (--dsize) >= 2);

	return arr;
}

int* reverse(int* const arr, const int size, Logging logging)
{
	for (int i = 0; i < size / 2; i++) {
		std::swap(arr[i], arr[size-1-i]);
	}

	if (logging == log_result)
		printArr(arr, size);

	return arr;
}