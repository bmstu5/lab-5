#include <iostream>
#include <ctime> // time
#include <cstdlib> // srand, rand, exit
#include <iomanip> // setw

#include "sort.h"

#define LEN(x) sizeof(x) / sizeof(*x)
#define CELL(x, y) std::left << std::setw(y) << x

int random(int a, int b)
{
	return a + (b - a + 1) * double(std::rand())/RAND_MAX;
}

int* randomize(int* arr, int size, Logging logging)
{	
	for (int p = 0; p < size; p++) {
		arr[p] = random(0, 256);
	}

	if (logging == log_result)
		printArr(arr, size);

	return arr;
}

bool is_sorted(int*arr, int size)
{
	for (int i = 0; i+1 < size; i++) {
		if (arr[i] > arr[i+1])
			return false;
	}
	return true;
}

void printStat(int* arr, int arrsize, int checked, int swapped)
{		
	std::cout << "  [size: " << CELL(arrsize, 5) << ", comparisons: "<< CELL(checked, 8) << ", swapped: " << CELL(swapped, 8) << "]\n";
	if (!is_sorted(arr, arrsize))
	{
		printArr(arr, arrsize);
		exit(1);
	}
}

int main()
{
	std::srand(std::time(nullptr));

	std::cout << "________PART 1:________\n";
	{
		int checked, swapped;
		int arrsize = 5;
		int arr[5];
		
		std::cout << "Random array:\n";
		randomize(arr, arrsize, log_result);

		std::cout << "Bubble Sort:\n";
		bubbleSort(arr, arrsize, log_each_swap, checked, swapped);
		printStat(arr, arrsize, checked, swapped);

		std::cout << "Bubble Sort Again:\n";
		bubbleSort(arr, arrsize, log_each_swap, checked, swapped);
		printStat(arr, arrsize, checked, swapped);

		std::cout << "Reverse:\n";
		reverse(arr, arrsize, log_result);

		std::cout << "Bubble Sort:\n";
		bubbleSort(arr, arrsize, log_each_swap, checked, swapped);
		printStat(arr, arrsize, checked, swapped);

		std::cout << "Random array:\n";
		randomize(arr, arrsize, log_result);

		std::cout << "Max Sort:\n";
		maxSort(arr, arrsize, log_each_swap, checked, swapped);
		printStat(arr, arrsize, checked, swapped);

		std::cout << "Max Sort Again:\n";
		maxSort(arr, arrsize, log_each_swap, checked, swapped);
		printStat(arr, arrsize, checked, swapped);

		std::cout << "Reverse:\n";
		reverse(arr, arrsize, log_result);

		std::cout << "Max Sort:\n";
		maxSort(arr, arrsize, log_each_swap, checked, swapped);
		printStat(arr, arrsize, checked, swapped);
	}

	std::cout << "\n________PART 2:________\n";

	// bubble sort
	for (int arrsize = 100; arrsize <= 10000; arrsize *= 10)
	{
		int checked, swapped;
		int* arr = new int[arrsize];
		
		randomize(arr, arrsize, no_logs);
		bubbleSort(arr, arrsize, no_logs, checked, swapped);
		std::cout << "bubble sort (random array)";
		printStat(arr, arrsize, checked, swapped);

		delete[] arr;
	}

	// max sort
	for (int arrsize = 100; arrsize <= 10000; arrsize *= 10)
	{
		int checked, swapped;
		int* arr = new int[arrsize];
		
		randomize(arr, arrsize, no_logs);
		maxSort(arr, arrsize, no_logs, checked, swapped);
		std::cout << "max sort (random array)   ";
		printStat(arr, arrsize, checked, swapped);

		delete[] arr;
	}

	return 0;
}

